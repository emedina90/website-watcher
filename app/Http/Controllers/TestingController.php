<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Factories\DeliveryServiceFactory;
use App\Factories\RankStrategyFactory;
use App\Models\Delivery;
use App\Strategies\Ranker;

class TestingController extends Controller
{
    public function sendEmail($id) {        
        $delivery = Delivery::findOrFail($id);
        $deliveryService = DeliveryServiceFactory::make(
            $delivery
        );

        $ranker = new Ranker(RankStrategyFactory::make( (int) $delivery->site_id));

        $dateFrom = new Carbon($delivery->relative_modifier);
        $dateFrom->minute = $dateFrom->second = 0;

        $items = $ranker->rank($dateFrom->toDateTimeString(), Carbon::now()->toDateTimeString(), (int) $delivery->limit);

        $deliveryService->setItems($items);

        \App\Jobs\Delivery::dispatch($deliveryService)->onQueue('deliveries');

        $delivery->last_sent_on = Carbon::now();
        $delivery->save();

        return "done";
    }
}
