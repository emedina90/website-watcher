<?php
namespace App\Services\HackerNews\Gateways;

use App\Contracts\Gateways\ItemGateway;
use App\Services\HttpClient;
use Illuminate\Support\Collection;

class HackerNewsItemGateway implements ItemGateway
{
    private $client;

    public function __construct(HttpClient $client)
    {
        $this->client = $client;
    }

    public function listTopItems(): Collection
    {
        $topStoryIds = $this->client->getTopStories();

        return $topStoryIds->map(function($id) {
            return $this->client->getItem( (int) $id);
        });
    }
}