<?php
namespace App\Services\HackerNews;

use App\Services\HttpClient;
use Illuminate\Support\Collection;

class HackerNewsClient extends HttpClient {

    protected $baseUri = 'https://hacker-news.firebaseio.com';
    protected $version = 'v0/';

    public function getTopStories() : Collection
    {
        $response = $this->request('get', $this->version . 'topstories.json');

        return collect(json_decode((string) $response->getBody(), true));
    }

    public function getItem(int $id) : array
    {
        $response = $this->request('get', $this->version . 'item/' . $id . '.json');

        return json_decode((string) $response->getBody(), true);
    }
}