<?php
namespace App\Services\HackerNews\Mappers;

use App\Contracts\Mappers\ItemMapper;
use App\Models\Item;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class HackerNewsItemMapper implements ItemMapper
{

    public function find()
    {
        // TODO: Implement find() method.
    }

    public function save(array $item) : Item
    {
        // TODO: Implement save() method.
    }

    public function saveMany(Collection $items, int $routineId, int $itemTypeId) : Collection
    {
        $items = $items->map(function ($item, $key) use ($routineId, $itemTypeId) {
            $hackerNewsItem = new Item;
            $hackerNewsItem->created_at   = Carbon::now();
            $hackerNewsItem->updated_at   = Carbon::now();
            $hackerNewsItem->routine_id   = $routineId;
            $hackerNewsItem->item_type_id = $itemTypeId;

            return $hackerNewsItem->fill($this->transform($item));
        });

        DB::table('items')->insert($items->toArray());

        return $items;
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    private function transform(array $item) : array
    {
        return [
            'score'        => array_get($item, 'score'),
            'by'           => array_get($item, 'by'),
            'dead'         => array_get($item, 'dead'),
            'url'          => array_get($item, 'url'),
            'title'        => array_get($item, 'title'),
            'external_id'  => array_get($item, 'id'),
        ];
    }
}