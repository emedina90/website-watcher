<?php

namespace App\Services\HackerNews;

use App\Contracts\Repositories\ItemRepository;
use App\Enumerations\HackerNews\HackerNewsItemTypes;
use App\Enumerations\SitesIds;
use App\Models\ItemType;
use App\Models\Routine;
use App\Services\Service;

class HackerNewsService implements Service
{
    private $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function processTopStoriesRoutine()
    {
        $routine = self::getCurrentRoutine(SitesIds::HACKER_NEWS);
        self::startNextRoutine(SitesIds::HACKER_NEWS);

        $itemType = ItemType::where([
            'site_id' => SitesIds::HACKER_NEWS,
            'type'    => HackerNewsItemTypes::STORY
        ])->first();

        $this->itemRepository->listTopItems( (int) $routine->id, (int) $itemType->id);
    }

    private static function getCurrentRoutine(int $siteId) : Routine
    {
        $routine = Routine::where('site_id', $siteId)
                          ->orderBy('created_at', 'desc')
                          ->first();

        if ( ! $routine)
        {
            $routine = self::startNextRoutine($siteId);
        }

        return $routine;
    }

    private static function startNextRoutine(int $siteId) : Routine
    {
        return Routine::create([
            'site_id' => $siteId
        ]);
    }
}