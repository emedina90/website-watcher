<?php
namespace App\Services\HackerNews\Repositories;

use App\Contracts\Gateways\ItemGateway;
use App\Contracts\Mappers\ItemMapper;
use App\Contracts\Repositories\ItemRepository;
use Illuminate\Support\Collection;

class HackerNewsItemRepository implements ItemRepository
{
    /**
     * @var ItemGateway
     */
    private $itemGateway;
    /**
     * @var ItemMapper
     */
    private $itemMapper;

    /**
     * HackerNewsItemRepository constructor.
     * @param ItemGateway $itemGateway
     * @param ItemMapper $itemMapper
     */
    public function __construct(ItemGateway $itemGateway, ItemMapper $itemMapper)
    {
        $this->itemGateway = $itemGateway;
        $this->itemMapper = $itemMapper;
    }

    /**
     * @return Collection
     */
    public function listTopItems(int $routineId, int $itemTypeId): Collection
    {
        $response = $this->itemGateway->listTopItems();

        return $this->itemMapper->saveMany($response, $routineId, $itemTypeId);
    }
}