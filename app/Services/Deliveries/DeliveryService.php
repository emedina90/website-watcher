<?php
namespace App\Services\Deliveries;

use Illuminate\Support\Collection;

interface DeliveryService
{
    public function send();

    public function setItems(Collection $items);
}