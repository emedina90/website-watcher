<?php
namespace App\Services\Deliveries;

use App\Mail\SimpleList;
use App\Models\Delivery;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use function var_dump;


/**
 * Class EmailDeliveryService
 * @package App\Services\Deliveries
 */
class EmailDeliveryService implements DeliveryService
{

    /**
     * @var Delivery
     */
    private $delivery;

    private $items;

    /**
     * EmailDeliveryService constructor.
     * @param Delivery $delivery
     */
    public function __construct(Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     *
     */
    public function send()
    {
        $email = $this->delivery->email;

        Mail::to($email->email)
            ->send(new SimpleList($this->items, 'Ranked Items'));
    }

    public function setItems(Collection $items)
    {
        $this->items = $items;
    }
}