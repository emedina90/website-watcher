<?php
namespace App\Services;

use GuzzleHttp\Client;

class HttpClient {
    protected $instance;

    protected $baseUri;

    public function request($method, $uri = '', array $options = [])
    {
        $client = $this->getInstance();

        return $client->request($method, $uri, $options);
    }

    protected function getInstance()
    {
        if ( ! isset($this->instance))
        {
            $this->instance = new Client([
                'base_uri' => $this->baseUri
            ]);
        }

        return $this->instance;
    }
}