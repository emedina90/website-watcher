<?php
namespace App\Services\Local\Repositories;

use App\Contracts\Repositories\ItemTypeRepository;
use App\Models\ItemType;

class LocalItemTypeRepository implements ItemTypeRepository
{
    public function create(array $attributes): ItemType
    {
        return ItemType::create($attributes);
    }
}