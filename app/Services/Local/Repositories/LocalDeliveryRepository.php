<?php
namespace App\Services\Local\Repositories;

use App\Contracts\Repositories\DeliveryRepository;
use App\Models\Delivery;

class LocalDeliveryRepository implements DeliveryRepository
{
    public function create(array $attributes): Delivery
    {
        return Delivery::create($attributes);
    }
}