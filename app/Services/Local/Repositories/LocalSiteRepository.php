<?php
namespace App\Services\Local\Repositories;

use App\Contracts\Repositories\SiteRepository;
use App\Models\Site;
use Illuminate\Support\Collection;

class LocalSiteRepository implements SiteRepository
{
    public function create(array $attributes): Site
    {
        return Site::create($attributes);
    }

    public function all(): Collection
    {
        return Site::all();
    }
}