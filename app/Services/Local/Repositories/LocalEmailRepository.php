<?php
namespace App\Services\Local\Repositories;

use App\Contracts\Repositories\EmailRepository;
use App\Models\Email;
use Illuminate\Support\Collection;

class LocalEmailRepository implements EmailRepository
{
    public function create(array $attributes): Email
    {
        return Email::create($attributes);
    }

    public function all(): Collection
    {
        return Email::all();
    }
}