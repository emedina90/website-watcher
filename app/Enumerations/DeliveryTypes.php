<?php
namespace App\Enumerations;

final class DeliveryTypes extends BaseEnumeration
{
    const EMAIL = 'email';
    const SLACK = 'slack';
    const TEXT  = 'text';
}