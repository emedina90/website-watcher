<?php
namespace App\Enumerations\HackerNews;

use App\Enumerations\BaseEnumeration;

final class HackerNewsItemTypes extends BaseEnumeration
{
    const STORY   = 'story';
    const COMMENT = 'comment';
    const ASK     = 'ask';
    const JOB     = 'job';
    const POLL    = 'poll';
}