<?php
namespace App\Enumerations;

use function array_values;
use ReflectionClass;

class BaseEnumeration
{
    public static function getConstants()
    {
        $reflectionClass = new ReflectionClass(static::class);
        $constants       = $reflectionClass->getConstants();

        return array_values($constants);
    }
}