<?php
namespace App\Contracts\Repositories;

use App\Models\Delivery;

interface DeliveryRepository
{
    public function create(array $attributes) : Delivery;
}