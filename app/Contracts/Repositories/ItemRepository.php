<?php
namespace App\Contracts\Repositories;

use Illuminate\Support\Collection;

interface ItemRepository {

    /**
     *
     * @return Collection
     */
    public function listTopItems(int $routineId, int $itemTypeId) : Collection;

 }