<?php
namespace App\Contracts\Repositories;

use App\Models\Email;
use Illuminate\Support\Collection;

interface EmailRepository
{
    public function create(array $attributes) : Email;

    public function all() : Collection;
}