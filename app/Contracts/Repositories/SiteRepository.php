<?php
namespace App\Contracts\Repositories;

use App\Models\Site;
use Illuminate\Support\Collection;

interface SiteRepository
{
    public function create(array $attributes) : Site;

    public function all() : Collection;
}