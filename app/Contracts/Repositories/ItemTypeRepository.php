<?php
namespace App\Contracts\Repositories;
use App\Models\ItemType;

interface ItemTypeRepository
{
    public function create(array $attributes) : ItemType;
}