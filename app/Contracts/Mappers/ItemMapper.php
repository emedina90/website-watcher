<?php
namespace App\Contracts\Mappers;

use App\Models\Item;
use Illuminate\Support\Collection;

interface ItemMapper {

        public function save(array $item) : Item;

        public function saveMany(Collection $items, int $routineId, int $itemTypeId) : Collection;

        public function find();

        public function delete();

}