<?php
namespace App\Contracts\Gateways;

use Illuminate\Support\Collection;

interface ItemGateway {

    public function listTopItems() : Collection;

}