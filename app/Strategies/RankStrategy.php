<?php
namespace App\Strategies;

use Illuminate\Support\Collection;

interface RankStrategy
{
    public function rank(string $dateFrom, string $dateTo, int $limit) : Collection;
}