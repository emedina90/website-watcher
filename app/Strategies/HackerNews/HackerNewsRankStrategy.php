<?php
namespace App\Strategies\HackerNews;

use App\Models\Routine;
use App\Strategies\RankStrategy;
use Carbon\Carbon;
use function collect;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use function implode;

class HackerNewsRankStrategy implements RankStrategy
{
    public function rank(string $dateFrom, string $dateTo, int $limit) : Collection
    {
        //get all routines between dates
        $routines   = Routine::whereBetween('created_at', [Carbon::parse($dateFrom), Carbon::parse($dateTo)])->get();

        $routineIds = $routines->pluck('id')->toArray();

        $innerSql = '(select * from items ';
        $innerSql.= 'where `routine_id` IN ('.implode(', ', $routineIds).') ';
        $innerSql.= 'ORDER BY `score` DESC) AS t1';

        $outerSql = 'select `external_id`, `by`, `url`, `title`, MAX(`score`) AS score from ' . $innerSql;
        $outerSql.= ' GROUP BY `external_id`,`by`, `url`, `title`';
        $outerSql.= ' ORDER BY `score` DESC LIMIT ' . $limit;

        $items = DB::select($outerSql);

        return collect($items);
    }
}