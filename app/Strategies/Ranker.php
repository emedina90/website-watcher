<?php
namespace App\Strategies;

class Ranker
{

    private $rankStrategy;

    public function __construct(RankStrategy $rankStrategy)
    {
        $this->rankStrategy = $rankStrategy;
    }

    public function rank(string $dateFrom, string $dateTo, int $limit)
    {
        return $this->rankStrategy->rank($dateFrom, $dateTo, $limit);
    }
}