<?php

namespace App\Console;

use App\Console\Commands\ConfigureDelivery;
use App\Console\Commands\ConfigureEmail;
use App\Console\Commands\ConfigureItemType;
use App\Console\Commands\ConfigureSite;
use App\Console\Commands\ExecuteDeliveries;
use App\Console\Commands\ExecuteServices;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        ExecuteServices::class,
        ExecuteDeliveries::class,
        ConfigureSite::class,
        ConfigureEmail::class,
        ConfigureDelivery::class,
        ConfigureItemType::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('execute:services')
                  ->cron('0 */3 * * *');

         $schedule->command('execute:deliveries')->daily();
    }

    /**\
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
