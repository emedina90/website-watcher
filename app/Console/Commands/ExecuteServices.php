<?php

namespace App\Console\Commands;

use App\Enumerations\SitesIds;
use App\Factories\ServiceFactory;
use App\Jobs\TopStoriesRoutine;
use App\Models\Site;
use function dispatch;
use Illuminate\Console\Command;
use Reflection;
use ReflectionClass;

class ExecuteServices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Executes all services.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // dispatch all services
        $sites = Site::all();

        //dispatch top stories job
        $sites->each(function ($site){
            $service = ServiceFactory::makeService( (int) $site->id);
            TopStoriesRoutine::dispatch($service)->onQueue('topstories');
        });
    }
}
