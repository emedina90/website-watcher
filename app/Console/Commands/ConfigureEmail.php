<?php

namespace App\Console\Commands;

use App\Contracts\Repositories\EmailRepository;
use Illuminate\Console\Command;

class ConfigureEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'configure:email {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an email record.';


    /**
     * @var EmailRepository $emailRepository
     */
    private $emailRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(EmailRepository $emailRepository)
    {
        parent::__construct();

        $this->emailRepository = $emailRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $this->emailRepository->create([
            'email' => $email
        ]);
    }
}
