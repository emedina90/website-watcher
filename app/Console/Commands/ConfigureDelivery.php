<?php

namespace App\Console\Commands;

use App\Contracts\Repositories\DeliveryRepository;
use App\Contracts\Repositories\EmailRepository;
use App\Contracts\Repositories\SiteRepository;
use App\Enumerations\DeliveryTypes;
use Illuminate\Console\Command;

class ConfigureDelivery extends Command
{
    const ITEM_LIMITS = [10, 20, 30, 40, 50];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'configure:delivery';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure a new delivery.';

    /**
     * @var EmailRepository
     */
    private $emailRepository;


    /**
     * @var SiteRepository
     */
    private $siteRepository;


    /**
     * @var DeliveryRepository
     */
    private $deliveryRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(EmailRepository $emailRepository, SiteRepository $siteRepository, DeliveryRepository $deliveryRepository)
    {
        parent::__construct();

        $this->emailRepository = $emailRepository;
        $this->siteRepository  = $siteRepository;
        $this->deliveryRepository = $deliveryRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = $this->emailRepository->all();

        if ($emails->count() === 0) {
            $this->error("No emails configured, please add some emails using the configure:email command.");
            exit(0);
        }

        $email = $this->choice("Select an email address", $emails->pluck('email')->toArray());

        $sites  = $this->siteRepository->all();

        if ($sites->count() === 0) {
            $this->error("No sites configured, please add a site using the configure:site command.");
        }

        $site = $this->choice("Select a site to deliver", $sites->pluck('name')->toArray());

        $deliveryType = $this->choice("Select a delivery type", DeliveryTypes::getConstants());

        $relativeModifier = $this->ask("How far back in time should items be collected? (Example: 1 weeks ago)");

        $limit = $this->choice("How many items would you like to limit this to?", self::ITEM_LIMITS);

        $deliverOn = $this->ask("When would you like this delivered? (Sun-Sat = 0-6 or enter Date string");

        $this->info("Creating delivery configuration...");

        $emailId = $emails->first(function($e) use ($email){
            return $e->email === $email;
        })->id;

        $siteId = $sites->first(function($s) use ($site){
            return $s->name === $site;
        })->id;

        $delivery =  $this->deliveryRepository->create([
            'email_id' => $emailId,
            'site_id'  => $siteId,
            'method'   => $deliveryType,
            'relative_modifier' => $relativeModifier,
            'limit' => $limit,
            'deliver_on' => $deliverOn
        ]);

        if ( !$delivery->exists ) {
            $this->error("Failed to create configuration.");
            exit(0);
        }

        $this->info("Configuration created!");
    }
}
