<?php

namespace App\Console\Commands;

use App\Contracts\Repositories\SiteRepository;
use Illuminate\Console\Command;

class ConfigureSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'configure:site {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add or remove a site.';


    /**
     * @var SiteRepository
     */
    private $siteRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SiteRepository $siteRepository)
    {
        parent::__construct();

        $this->siteRepository = $siteRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $this->siteRepository->create([
            'name' => $name
        ]);
    }
}
