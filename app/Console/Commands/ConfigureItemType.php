<?php

namespace App\Console\Commands;

use App\Contracts\Repositories\ItemTypeRepository;
use App\Contracts\Repositories\SiteRepository;
use Illuminate\Console\Command;

class ConfigureItemType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'configure:ItemType';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure an item type.';

    /**
     * @var ItemTypeRepository
     */
    private $itemTypeRepository;

    /**
     * @var SiteRepository
     */
    private $siteRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ItemTypeRepository $itemTypeRepository, SiteRepository $siteRepository)
    {
        parent::__construct();

        $this->itemTypeRepository = $itemTypeRepository;
        $this->siteRepository     = $siteRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 1. Get all sites, select one
        $sites = $this->siteRepository->all();

        if ($sites->count() === 0) {
            $this->error("No sites exist, please configure one using configure:site.");
            exit(0);
        }

        $site = $this->choice("Select as site", $sites->pluck('name')->toArray());

        // 2. Ask for the item type, save
        $type = $this->ask("What is the item type for this site?");

        $this->info("Creating Item Type...");

        $siteId = $sites->first(function($s) use ($site) {
            return $s->name === $site;
        })->id;
        $itemType = $this->itemTypeRepository->create([
            'site_id' => $siteId,
            'type'    => $type
        ]);

        if ( ! $itemType->exists ) {
            $this->error("Unable to create item type!");
            exit(0);
        }

        $this->info("Item type created!");
    }
}
