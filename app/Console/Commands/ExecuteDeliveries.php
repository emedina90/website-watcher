<?php

namespace App\Console\Commands;

use App\Factories\DeliveryServiceFactory;
use App\Factories\RankStrategyFactory;
use App\Models\Delivery;
use App\Services\DeliveryService;
use App\Strategies\Ranker;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ExecuteDeliveries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:deliveries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scans for deliveries to be sent.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $now->minute = $now->second = 0;

        // TODO: Buildin support to use the frequency column, right now it assume all records are on a daily schedule
        $deliveries = Delivery::all();

        $deliveries->each(function(Delivery $delivery){
            $deliveryService = DeliveryServiceFactory::make($delivery);

            $ranker = new Ranker(RankStrategyFactory::make( (int) $delivery->site_id));

            $dateFrom = new Carbon($delivery->relative_modifier);
            $dateFrom->minute = $dateFrom->second = 0;

            $items = $ranker->rank($dateFrom->toDateTimeString(), Carbon::now()->toDateTimeString(), (int) $delivery->limit);

            $deliveryService->setItems($items);

            \App\Jobs\Delivery::dispatch($deliveryService)->onQueue('deliveries');

            $delivery->last_sent_on = Carbon::now()->toDateString();
            $delivery->save();
        });
        return;
    }
}
