<?php

namespace App\Providers;

use App\Contracts\Gateways\ItemGateway;
use App\Contracts\Mappers\ItemMapper;
use App\Contracts\Repositories\ItemRepository;
use App\Services\HackerNews\Gateways\HackerNewsItemGateway;
use App\Services\HackerNews\HackerNewsClient;
use App\Services\HackerNews\HackerNewsService;
use App\Services\HackerNews\Mappers\HackerNewsItemMapper;
use App\Services\HackerNews\Repositories\HackerNewsItemRepository;
use Illuminate\Support\ServiceProvider;

class HackerNewsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(HackerNewsItemRepository::class)
                  ->needs(ItemGateway::class)
                  ->give(function() {
                      $client = new HackerNewsClient;
                      return new HackerNewsItemGateway($client);
                  });

        $this->app->when(HackerNewsItemRepository::class)
                  ->needs(ItemMapper::class)
                  ->give(function() {
                      return new HackerNewsItemMapper;
            });
    }
}
