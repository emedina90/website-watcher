<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\Repositories\SiteRepository',
            'App\Services\Local\Repositories\LocalSiteRepository'
        );

        $this->app->bind(
            'App\Contracts\Repositories\EmailRepository',
            'App\Services\Local\Repositories\LocalEmailRepository'
        );

        $this->app->bind(
            'App\Contracts\Repositories\DeliveryRepository',
            'App\Services\Local\Repositories\LocalDeliveryRepository'
        );

        $this->app->bind(
            'App\Contracts\Repositories\ItemTypeRepository',
            'App\Services\Local\Repositories\LocalItemTypeRepository'
        );
    }
}
