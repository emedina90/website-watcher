<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'score',
        'by',
        'dead',
        'url',
        'title',
        'external_id'
    ];

    //
    public function routine()
    {
        return $this->belongsTo(Routine::class);
    }

    public function ItemType()
    {
        return $this->belongsTo(ItemType::class);
    }
}
