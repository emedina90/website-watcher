<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = ['name'];

    public function routines()
    {
        return $this->hasMany(Routine::class);
    }

    public function itemTypes()
    {
        return $this->hasMany(ItemType::class);
    }

    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }
}
