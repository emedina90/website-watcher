<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Routine extends Model
{
    protected $fillable = [
        'site_id'
    ];

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
