<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $fillable = [
        'site_id',
        'email_id',
        'method',
        'relative_modifier',
        'limit',
        'deliver_on'
    ];

    public function email()
    {
        return $this->belongsTo(Email::class);
    }

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
