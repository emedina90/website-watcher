<?php

namespace App\Models;

use App\Services\DeliveryService;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{

    protected $fillable = [
        'email'
    ];

    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }
}
