<?php
namespace App\Factories;

use App\Enumerations\DeliveryTypes;
use App\Factories\Exceptions\ServiceNotFoundException;
use App\Services\Deliveries\EmailDeliveryService;

class DeliveryServiceFactory
{
    public static function make($delivery)
    {
        switch ($delivery->method)
        {
            case DeliveryTypes::EMAIL:
                return new EmailDeliveryService($delivery);
            default:
                throw new ServiceNotFoundException('Service not found');
        }
    }
}