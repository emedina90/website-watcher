<?php
namespace App\Factories;

use App\Enumerations\SitesIds;
use App\Factories\Exceptions\ServiceNotFoundException;
use App\Services\HackerNews\HackerNewsService;
use App\Services\HackerNews\Repositories\HackerNewsItemRepository;

class ServiceFactory
{
    public static function makeService(int $serviceId)
    {
        switch ($serviceId)
        {
            case SitesIds::HACKER_NEWS:
                return new HackerNewsService(resolve(HackerNewsItemRepository::class));
            default:
                throw new ServiceNotFoundException('Service not found.');
        }
    }
}