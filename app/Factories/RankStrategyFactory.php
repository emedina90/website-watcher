<?php
namespace App\Factories;

use App\Enumerations\SitesIds;
use App\Factories\Exceptions\StrategyNotFoundException;
use App\Strategies\HackerNews\HackerNewsRankStrategy;
use App\Strategies\Ranker;
use Illuminate\Support\Facades\App;

class RankStrategyFactory
{
    public static function make(int $siteId)
    {
        switch($siteId)
        {
            case SitesIds::HACKER_NEWS:
                return App::make(HackerNewsRankStrategy::class);
            default:
                throw new StrategyNotFoundException('Strategy not found.');
        }
    }
}