# HN Watch

A simple application that sends a list of Hacker News articles ranked by points over some defined period of time.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

Clone the repository

```
git clone git@gitlab.com:emedina90/website-watcher.git
```

Install dependencies

```
composer install
```

Run migrations

```
php artisan migrate
```

Make sure the Laravel CRON is enabled

```
* * * * * php /path/to/project/artisan schedule:run 1>> /dev/null 2>&1
```

### Configure the Hacker News site

```
php artisan configure:site "Hacker News"
php artisan configure:ItemType
```

Follow the prompts and enter "story" as the first item type

### Configure an Email

```
php artisan configure:email "example@example.com"
```

### Configure a delivery schedule

```
php artisan configure:delivery
```

Follow the prompts to select the Hacker News site and your configured Email address