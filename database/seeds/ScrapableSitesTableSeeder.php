<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScrapableSitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = DB::table('scrapable_site_definitions')->insertGetId([
            'type' => 'api',
            'url'  => 'https://hacker-news.firebaseio.com/v0/'
        ]);

        DB::table('scrapable_sites')->insert([
            'scrapable_site_definition_id' => $id,
            'name' => 'Hacker News',
            'description' => 'Scrape Hacker News using it\'s public API.',
        ]);
    }
}
