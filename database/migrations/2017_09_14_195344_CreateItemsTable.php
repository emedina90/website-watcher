<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('routine_id')->nullable();
            $table->integer('item_type_id')->nullable();
            $table->integer('score')->nullable();
            $table->text('by')->nullable();
            $table->boolean('dead')->nullable();
            $table->text('url')->nullable();
            $table->text('title')->nullable();
            $table->string('external_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('items');
    }
}
