<html>
<head></head>
<body>
<h1>{{$title}}</h1>

<style>
    table.simple-table{
        border: 1px solid #dedede;
    }

    table.simple-table td {
        border: 1px solid #dedede;
    }
</style>

<table class="simple-table">
    <tr>
        <th>Score</th>
        <th>Title</th>
        <th>Comments Section</th>
    </tr>

    @foreach($items as $item)
        <tr>
            <td>{{$item->score}}</td>
            <td><a href="{{$item->url}}">{{$item->title}}</a></td>
            <td><a href="https://news.ycombinator.com/item?id={{$item->external_id}}">View Comments</a></td>
        </tr>
    @endforeach
</table>

</body>
</html>